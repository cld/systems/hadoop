# Build Instructions

To build the instrumented version of Hadoop on Linux (Debian),
please follow the following Build Instructions.

## Dependencies

To build the instrumented hadoop, the following dependencies need to be installed.

### General

To install things like git, ant,vim,  etc please follow the the following instructions

```
sudo apt-get install vim git ant curl g++ make cmake -y 
```

### Java

At the time of writing, Hadoop specifically needs JDK 1.8.  Check your Java version with `java -version`.  The default Java version on your machine is probably Java 11, so you will need to install Java 8.

JDK 1.8 is deprecated in a lot of OS's and installing it can be tricky.  At the time of writing, the following commands work on the MPI Debian machines.

```
sudo tar -zxvf /home/jcmace/jdk-8u241-linux-x64.tar.gz -C /usr/lib/jvm
sudo update-alternatives --install /usr/bin/java java /usr/lib/jvm/jdk1.8.0_241/bin/java 3
sudo update-alternatives --config java
```

The final command will leave you at a prompt where you can select JDK 1.8:
```
  Selection    Path                                         Priority   Status
------------------------------------------------------------
* 0            /usr/lib/jvm/java-11-openjdk-amd64/bin/java   1111      auto mode
  1            /usr/lib/jvm/java-11-openjdk-amd64/bin/java   1111      manual mode
  2            /usr/lib/jvm/jdk1.8.0_241/bin/java            3         manual mode

Press <enter> to keep the current choice[*], or type selection number:
```


### Maven

Maven is used for installing all the tracing repositories and the instrumented versions of systems.
To install maven, please follow the following instructions

```
sudo add-apt-repository ppa:andrei-pozolotin/maven3 -y
sudo apt-get update
sudo apt-get install maven3 -y 
```

### Protobuf

To install protobuf, please follow the following instructions

```
wget https://github.com/google/protobuf/releases/download/v2.5.0/protobuf-2.5.0.tar.gz
gunzip protobuf-2.5.0.tar.gz
tar -xvf protobuf-2.5.0.tar
cd protobuf-2.5.0
./configure --prefix=/usr
make
sudo make install 
```

### Flatbuffers

To install flatbuffers, please follow the following instructions

```
git clone https://github.com/google/flatbuffers.git
cd flatbuffers
git checkout tags/v1.3.0
cmake -G "Unix Makefiles"
make
sudo make install 
```

### Scala

To install Scala, please follow the following instructions

```
wget https://downloads.lightbend.com/scala/2.10.5/scala-2.10.5.deb
sudo apt-get -f install -y
sudo dpkg -i scala-2.10.5.deb
sudo apt-get -f install -y 
```

### AspectJ

To install AspectJ, please follow the following instructions

```
wget http://mirror.csclub.uwaterloo.ca/eclipse/tools/aspectj/aspectj-1.8.7.jar
java -jar aspectj-1.8.7.jar 
```

## Repositories

For building Hadoop, 3 repositories are needed which can be downloaded as follows

```
git clone https://gitlab.mpi-sws.org/cld/tracing/tracing.git
git clone https://github.com/tracingplane/tracingplane-java.git
git clone https://gitlab.mpi-sws.org/cld/systems/hadoop.git 
```

### Environment

+ `JAVA_HOME` must be set to the folder that the JDK is containted withing (eg: /usr/lib/jvm/oracle-java8-jdk-amd64)
+ Add `$JAVA_HOME/bin` folder to your `$PATH` variable
+ Add `path/to/aspectj1.8/bin` folder to your `$PATH` variable
+ Add `path/to/aspectj1.8/lib/aspectjrt.jar` to `$CLASSPATH` variable

### Building

Specific branches of the repositories must be built in a specified order.
The tracing and tracingplane-java repositories must be built before Hadoop is built.

#### Tracingplane-java

```
git checkout v0.1
mvn clean package install -DskipTests
```

#### Tracing

```
git checkout tracingplane
# Try git checkout --track origin/tracingplane if the above command doesn't work.
mvn clean package install -DskipTests
```

#### Hadoop

```
git checkout brownsys-tracingplane-2.7.2
mvn clean package install -Pdist -DskipTests -Dmaven.javadoc.skip="true"
```

## Configuring HDFS

The following instructions will configure a minimally working version of HDFS. From the base directory for the Hadoop git repo,
the built version of hadoop will be located in `hadoop-dist/target/hadoop-2.7.2`. Within the build directory, `etc/hadoop` will
contain the default config files. Copy this directory to somewhere outside of the HDFS build directory, otherwise any changes made to
the config file will be overwritten anytime HDFS is rebuilt.

The config directory should contain `core-site.xml` and `hdfs-site.xml`.
In addition, an `application.conf` file should also reside in a folder that is part of the `HADOOP_CLASSPATH` environment variable

### core-site.xml

Here is a starter file

```
<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="configuration.xsl"?>
<configuration>
  <property> 
    <name>fs.default.name</name> 
    <value>hdfs://127.0.0.1:9000</value> 
  </property>
</configuration>
```

### hdfs-site.xml

Here is a starter file. Replace LOG-DIR with your data directory

```
<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="configuration.xsl"?>
<configuration>
  <property>
    <name>dfs.datanode.data.dir</name>
    <value>/home/vanand/MPI/hadoop/hdfs-data</value>
  </property>
  <property> 
    <name>dfs.namenode.safemode.extension</name> 
    <value>0</value> 
  </property>
  <property> 
    <name>dfs.client-write-packet-size</name> 
    <value>65536</value> 
  </property>
  <property> 
    <name>dfs.namenode.handler.count</name> 
    <value>1</value> 
  </property>
</configuration>
```

### application.conf

Here is a sample conf file. Note that you must set the `xtrace.server.datastore.dir` to some location on your machine.
For the traces to be saved at this location, this location must be part of the `CLASSPATH_PREFIX` variable. Otherwise,
the traces will get stored at the location from which the xtrace server is launched.

```
tracingplane.transit-layer.factory = "edu.brown.cs.systems.tracingplane.atom_layer.AtomLayerFactory$TransitLayerFactoryImpl"
tracingplane.atom-layer.factory = "edu.brown.cs.systems.tracingplane.baggage_layer.BaggageLayerFactory$AtomLayerFactoryImpl"
tracingplane.baggage-layer.factory = "edu.brown.cs.systems.tracingplane.baggage_buffers.BaggageLayerFactoryImpl"

xtrace.server.datastore.dir = "/highway/to/hell"

retro.throttling.queuetype = "locking"
retro.throttling.default-scheduler = "none"

# By default, all reporting is on
xtrace.client.reporting.on = true
xtrace.client.reporting.default = true
xtrace.client.reporting.discoverymode = false       # not on for now
xtrace.client.recycle-threshold = 10000             # effectively disable recycling

# don't trace main methods (eg DataNode main, NameNode main, etc.)
xtrace.client.tracemain = true
xtrace.client.tracemain_level = "info"

# disable retro
resource-reporting.aggregation.active = false

#xtrace.client.reporting.disabled = [ "edu.brown.cs.systems.xtrace.logging.BaggageWrappers", "Queue", "MonitorLock", "ReadWriteLock", "Network", "Disk", "Execution-Branch", "Execution-Join", "Execution-Sleep" ]

#xtrace.client.reporting.disabled = [ "edu.brown.cs.systems.xtrace.logging.BaggageWrappers", "Queue", "MonitorLock", "ReadWriteLock", "Network", "Disk", "Execution-Branch", "Execution-Join", "Execution-Sleep", "Execution-CPU" ]

# Special bullshit because i want to be able to test using HDFS shell
hadoop.xtrace.trace_fsshell = true


# Exp1 Spark settings
xtrace.client.reporting.default_level = "debug"
xtrace.client.reporting.report_baggage = true
xtrace.client.reporting.report_baggage_size = true
xtrace.client.reporting.log_baggage_access = true
xtrace.client.reporting.log_baggage_access_causality = true
xtrace.client.reporting.log_baggage_access_as_separate_trace = true
baggage-buffers.access-listener = "edu.brown.cs.systems.xtrace.wrappers.XTraceBaggageAccessListener"
transit-layer.access-listener = "edu.brown.cs.systems.xtrace.wrappers.XTraceBaggageAccessListener"
retro.baggage.allow_disk_reads_in_baggage = false
retro.baggage.count_disk_reads_in_baggage_default = false
retro.baggage.allow_disk_writes_in_baggage = false
retro.baggage.count_disk_writes_in_baggage_default = false
resource-tracing.reporting.report-disk-as-auxiliary = false
resource-tracing.reporting.report-network-as-auxiliary = false
baggage-buffers.compaction_enabled = false

pubsub { 
 server {
   hostname = "localhost"
   bindto = "0.0.0.0"
   port = 5563
 }  
 topics {
   control = "_"
 }
}
```

### Environment

+ `HADOOP_HOME` should be set to the build directory, eg hadoop-dist/target/hadoop-2.7.2
+ `HADOOP_CONF_DIR` should be set to the location where the HDFS config files were copied
+ `HADOOP_CLASSPATH` must contain `path/to/tracing/dist/target/*` and `path/to/folder/with/application.conf`
+ `CLASSPATH_PREFIX` must be set to `path/to/folder/with/application.conf`
+ `LOG_DIR` can be set to a directory where all the logs will be stored`

## Running XTrace PubSub


All of the instrumentation libraries use a pub sub system to communicate.  Agents running in the instrumented Hadoop will receive commands over pubsub, and send their output back over the pubsub.  The pubsub project is in `tracingplane/pubsub`

The following command will start an X-Trace server which also includes a pub sub server:

	tracingplane/pubsub/target/appassembler/bin/server

To check which Java process are running, use the `jps` command.  This is an easy way to check if something has crashed.  You should expect to see a process called `StandaloneServer`.

## Running XTrace Backend

The PubSub can instead be run using the backend command which also starts a visualization server at localhost:4080.

This can be started as follows:

```
tracing/xtrace/server/target/appassembler/bin/backend
```

## Start HDFS

From the base directory for the Hadoop git repository, the built version of hadoop will be located in `hadoop-dist/target/hadoop-2.7.2`.  Within the build directory, `bin` contains various command line utilities, while `sbin` contains some useful scripts for starting and stopping processes.  Before starting HDFS, we must format its data dir:

```
> ${HADOOP_HOME}/bin/hdfs --config $HADOOP_CONF_DIR namenode -format
> ${HADOOP_HOME}/bin/hdfs --config $HADOOP_CONF_DIR datanode -format
```

Then start an HDFS NameNode and HDFS DataNode:

```
> ${HADOOP_HOME}/sbin/hadoop-daemon.sh start namenode
> ${HADOOP_HOME}/sbin/hadoop-daemon.sh start datanode
```

You should expect to see messages in the output stream along the lines of the following:

	Pivot Tracing initialized
	Resource reporting executor started
	ZmqReporter: QUEUE-disk -> queue
	ZmqReporter: DISK- -> disk
	ZmqReporter: CPU- -> cpu
	/META-INF/lib/libthreadcputimer.dylib extracted to temporary file /var/folders/d3/38f0syys5yjcvynz8p4n3t4w0000gn/T/jni_file_2410736199844535966.dll

Again, check which Java processes are running using the `jps` command.  In addition to `StandaloneServer`, you should expect to see `NameNode` and `DataNode`
For reference, HDFS runs a Web UI by default at [localhost:50070](http://localhost:50070).

### HDFS Start script

For convenience, you can source the following script to be able to start the HDFS nodes easily.
You must set the environment variables `LOG_BASE_DIR`, `TMP_BASE_DIR`, and `PID_BASE_DIR` to a directory where you want all the logs to be collected.

Here is the script

```
hdfs() {  
  command="$1";
  shift;
  if [[ $command = namenode ]]; then
    NAMENODE_COMMAND=$1 # One of "start" or "stop"
    if [[ -z $NAMENODE_COMMAND ]] ; then
        NAMENODE_COMMAND="start"
    fi
    # Start NameNode
    OPTS_ORIG=$HADOOP_OPTS
    export HADOOP_LOG_DIR=${LOG_BASE_DIR}/namenode
    export HADOOP_PID_DIR=${PID_BASE_DIR}/namenode
    export HADOOP_OPTS="-Dhadoop.tmp.dir=${TMP_BASE_DIR}/namenode ${OPTS_ORIG}"
    ${HADOOP_HOME}/sbin/hadoop-daemon.sh $NAMENODE_COMMAND namenode;
    export HADOOP_OPTS=$OPTS_ORIG
    return
  fi
  if [[ $command = datanode ]]; then
    DATANODE_COMMAND=$1
    DATANODE_NUMBER=$2
    if [[ -z $DATANODE_COMMAND ]] ; then
        DATANODE_COMMAND="start"
    fi
    if [[ -z $DATANODE_NUMBER ]] ; then 
        DATANODE_NUMBER=1;
    fi
    # Start a datanode
    OPTS_ORIG=$HADOOP_OPTS
    export HADOOP_LOG_DIR=${LOG_BASE_DIR}/datanode_${DATANODE_NUMBER}
    export HADOOP_PID_DIR=${PID_BASE_DIR}/datanode_${DATANODE_NUMBER}
    export HADOOP_OPTS="-Dhadoop.tmp.dir=${TMP_BASE_DIR}/datanode_${DATANODE_NUMBER} ${OPTS_ORIG}"
    DN_PROPS="-Ddfs.datanode.address=0.0.0.0:5001$i \
              -Ddfs.datanode.http.address=0.0.0.0:5008$i \
              -Ddfs.datanode.ipc.address=0.0.0.0:5002$i"
    ${HADOOP_HOME}/sbin/hadoop-daemon.sh $DATANODE_COMMAND datanode $DN_PROPS
    export HADOOP_OPTS=$OPTS_ORIG
    return
  fi
  if [[ $command = start ]] ; then
    NUM_DATANODES=$1;
    if [[ -z $NUM_DATANODES ]] ; then
      NUM_DATANODES=3;
    fi
    echo "===== Starting hdfs with $NUM_DATANODES datanodes =====";

    # Start NameNode
    hdfs namenode start

    for i in $(seq $NUM_DATANODES) 
    do
        hdfs datanode start $i
    done

    return
  fi
 if [[ $command = stop ]] ; then
    NUM_DATANODES=$1;
    if [[ -z $NUM_DATANODES ]] ; then
      NUM_DATANODES=3;
    fi
    echo "===== Stopping hdfs with $NUM_DATANODES datanodes =====";

    # Start NameNode
    hdfs namenode stop

    for i in $(seq $NUM_DATANODES) 
    do
        hdfs datanode stop $i
    done

    return
 fi
 if [[ $command = format ]]; then
    echo "Really delete all data and reformat HDFS? (y/n)";
    read ans;
    if [[ $ans = y ]]; then 
        echo "Reformatting HDFS...";
        rm -rf ${LOG_BASE_DIR}/*
        rm -rf ${PID_BASE_DIR}/*
        rm -rf ${TMP_BASE_DIR}/*

        OPTS_ORIG=$HADOOP_OPTS
        export HADOOP_LOG_DIR=${LOG_BASE_DIR}/namenode
        export HADOOP_PID_DIR=${PID_BASE_DIR}/namenode
        export HADOOP_OPTS="-Dhadoop.tmp.dir=${TMP_BASE_DIR}/namenode ${OPTS_ORIG}"
        ${HADOOP_HOME}/bin/hdfs --config $HADOOP_CONF_DIR namenode -format "$@"; 
        export HADOOP_OPTS=$OPTS_ORIG
    else 
        echo "Reformat aborted."; 
    fi
    return;
 fi
 if [[ $command = build ]]; then 
    echo "===== Building ${HADOOP_GIT} =====";
    echo "mvn clean package install -Pdist -DskipTests -Dmaven.javadoc.skip=\"true\"";
    mvn -f ${HADOOP_GIT}/pom.xml clean package install -Pdist -DskipTests -Dmaven.javadoc.skip="true" "$@"; 
    return;
 fi
 ${HADOOP_HOME}/bin/hdfs $command "$@"
}
```

To format the directories, run the following command

```
hdfs format
``` 

To run the nodes, you can run the following commands

```
hdfs namenode start
hdfs datanode start
```

To stop the nodes, run the following command

```
hdfs stop
```

## Generating Traces

Traces can be generating by performing simple filesystem operations on hdfs.

An example is as follows

```
${HADOOP_HOME}/bin/hadoop fs -ls <name_of_dir>
```

Full list of operations can be found here : [operations](https://hadoop.apache.org/docs/r2.7.2/hadoop-project-dist/hadoop-common/FileSystemShell.html)
