# Build Instructions

To build the instrumented version of Hadoop on macOS,
please follow the following Build Instructions.

## Dependencies

To build the instrumented hadoop, the following dependencies need to be installed.

### General

To install required packages (git, ant, vim, curl, make, cmake, maven, scala, aspectj, wget) first install homebrew.

```
$ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

To install packages run the following command

```
$ brew install XXX
```

### Java

To install java, please download jdk from the following link and follow the instructions. It is very important that you get the right version of Java. To download from this link you need to make an oracle account because it is a legacy version.

```
https://download.oracle.com/otn/java/jdk/8u241-b07/1f5b5a70bf22433b84d0e960903adac8/jdk-8u241-macosx-x64.dmg
```

To check you have the right version installed run

```
$ java -version
```

and your output should be

```
java version "1.8.0_XXX"
```

### Protobuf

To install protobuf, follow the following instructions

```
$ wget https://github.com/google/protobuf/releases/download/v2.5.0/protobuf-2.5.0.tar.gz
$ gunzip protobuf-2.5.0.tar.gz
$ tar -xvf protobuf-2.5.0.tar
$ cd protobuf-2.5.0
$ ./configure --prefix=/usr
$ make
$ sudo make install 
```

### Flatbuffers

To install flatbuffers, please follow the following instructions

```
$ git clone https://github.com/google/flatbuffers.git
$ cd flatbuffers
$ git checkout tags/v1.3.0
$ cmake -G "Unix Makefiles"
$ make
$ sudo make install 
```

## Repositories

For building Hadoop three repositories are needed which can be downloaded as follows. Navigate to your directory for building hadoop and run the following commands there

```
$ git clone https://gitlab.mpi-sws.org/cld/tracing/tracing.git
$ git clone https://github.com/tracingplane/tracingplane-java.git
$ git clone https://gitlab.mpi-sws.org/cld/systems/hadoop.git 
```

### Environment

+ `JAVA_HOME` must be set to the folder that the JDK is containted withing (eg: /Library/Java/JavaVirtualMachines/jdk1.8.0_241.jdk/Contents/Home)
+ Add `$JAVA_HOME/bin` folder to your `$PATH` variable
+ Add `path/to//aspectj/bin` folder to your `$PATH` variable
+ Add `path/to//aspectj/lib/aspectjrt.jar` to `$CLASSPATH` variable

The easiest way is to change your bash_profile using the following command

```
$ nano ~/.bash_profile
```

Then make the changes to your bash_profile and save it for the current session using the following command

```
$ source ~/.bash_profile
```

Here is a sample of what you need to add to bash_profile:

```
export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_241.jdk/Contents/Home

export PATH=$PATH:$JAVA_HOME/bin:/usr/local/opt/aspectj/libexec/aspectj/bin

export CLASS_PATH=/usr/local/opt/aspectj/libexec/aspectj/lib/aspectjrt.jar
```

### Building

Specific branches of the repositories must be built in a specified order.
The tracing and tracingplane-java repositories must be built before Hadoop is built.

To check the current git branch, use the following command

```
$ git branch
```

To change the git branch, use the following command

```
$ git checkout XXX
```

#### Tracingplane-java

```
$ git checkout v0.1
$ mvn clean package install -DskipTests
```

#### Tracing

```
$ git checkout tracingplane
# Try git checkout --track origin/tracingplane if the above command doesn't work.
$ mvn clean package install -DskipTests
```

#### Hadoop

```
$ git checkout brownsys-tracingplane-2.7.2
$ mvn clean package install -Pdist -DskipTests -Dmaven.javadoc.skip="true"
```

## Configuring HDFS

The following instructions will configure a minimally working version of HDFS. From the base directory for the Hadoop git repo,
the built version of hadoop will be located in `hadoop-dist/target/hadoop-2.7.2`. Within the build directory, `etc/hadoop` will
contain the default config files. Copy this directory to somewhere outside of the HDFS build directory, otherwise any changes made to
the config file will be overwritten anytime HDFS is rebuilt.

The config directory should contain `core-site.xml` and `hdfs-site.xml`.
In addition, an `application.conf` file should also reside in a folder that is part of the `HADOOP_CLASSPATH` environment variable

### core-site.xml

Change the content of the existing file to the following

```
<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="configuration.xsl"?>
<configuration>
  <property> 
    <name>fs.default.name</name> 
    <value>hdfs://127.0.0.1:9000</value> 
  </property>
</configuration>
```

### hdfs-site.xml

Change the content of the existing file to the following. Replace LOG_DIR with your data directory

```
<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="configuration.xsl"?>
<configuration>
  <property>
    <name>dfs.datanode.data.dir</name>
    <value>/LOG_DIR/hadoop/hdfs-data</value>
  </property>
  <property> 
    <name>dfs.namenode.safemode.extension</name> 
    <value>0</value> 
  </property>
  <property> 
    <name>dfs.client-write-packet-size</name> 
    <value>65536</value> 
  </property>
  <property> 
    <name>dfs.namenode.handler.count</name> 
    <value>1</value> 
  </property>
</configuration>
```

### application.conf

Here is a sample conf file. Note that you must set the `xtrace.server.datastore.dir` to some location on your machine.
For the traces to be saved at this location, this location must be part of the `CLASSPATH_PREFIX` variable. Otherwise,
the traces will get stored at the location from which the xtrace server is launched.

Make a directory for your output files and replace LOG_DIR with your directory

```
tracingplane.transit-layer.factory = "edu.brown.cs.systems.tracingplane.atom_layer.AtomLayerFactory$TransitLayerFactoryImpl"
tracingplane.atom-layer.factory = "edu.brown.cs.systems.tracingplane.baggage_layer.BaggageLayerFactory$AtomLayerFactoryImpl"
tracingplane.baggage-layer.factory = "edu.brown.cs.systems.tracingplane.baggage_buffers.BaggageLayerFactoryImpl"

xtrace.server.datastore.dir = "/LOG_DIR/hadoop/hdfs-data/output"

retro.throttling.queuetype = "locking"
retro.throttling.default-scheduler = "none"

# By default, all reporting is on
xtrace.client.reporting.on = true
xtrace.client.reporting.default = true
xtrace.client.reporting.discoverymode = false       # not on for now
xtrace.client.recycle-threshold = 10000             # effectively disable recycling

# don't trace main methods (eg DataNode main, NameNode main, etc.)
xtrace.client.tracemain = true
xtrace.client.tracemain_level = "info"

# disable retro
resource-reporting.aggregation.active = false

#xtrace.client.reporting.disabled = [ "edu.brown.cs.systems.xtrace.logging.BaggageWrappers", "Queue", "MonitorLock", "ReadWriteLock", "Network", "Disk", "Execution-Branch", "Execution-Join", "Execution-Sleep" ]

#xtrace.client.reporting.disabled = [ "edu.brown.cs.systems.xtrace.logging.BaggageWrappers", "Queue", "MonitorLock", "ReadWriteLock", "Network", "Disk", "Execution-Branch", "Execution-Join", "Execution-Sleep", "Execution-CPU" ]

# Special bullshit because i want to be able to test using HDFS shell
hadoop.xtrace.trace_fsshell = true


# Exp1 Spark settings
xtrace.client.reporting.default_level = "debug"
xtrace.client.reporting.report_baggage = true
xtrace.client.reporting.report_baggage_size = true
xtrace.client.reporting.log_baggage_access = true
xtrace.client.reporting.log_baggage_access_causality = true
xtrace.client.reporting.log_baggage_access_as_separate_trace = true
baggage-buffers.access-listener = "edu.brown.cs.systems.xtrace.wrappers.XTraceBaggageAccessListener"
transit-layer.access-listener = "edu.brown.cs.systems.xtrace.wrappers.XTraceBaggageAccessListener"
retro.baggage.allow_disk_reads_in_baggage = false
retro.baggage.count_disk_reads_in_baggage_default = false
retro.baggage.allow_disk_writes_in_baggage = false
retro.baggage.count_disk_writes_in_baggage_default = false
resource-tracing.reporting.report-disk-as-auxiliary = false
resource-tracing.reporting.report-network-as-auxiliary = false
baggage-buffers.compaction_enabled = false

pubsub { 
 server {
   hostname = "localhost"
   bindto = "0.0.0.0"
   port = 5563
 }  
 topics {
   control = "_"
 }
}
```

### Environment

+ `HADOOP_HOME` should be set to the build directory, eg hadoop-dist/target/hadoop-2.7.2
+ `HADOOP_CONF_DIR` should be set to the location where the HDFS config files were copied
+ `HADOOP_CLASSPATH` must contain `path/to/tracing/dist/target/*` and `path/to/folder/with/application.conf`
+ `CLASSPATH_PREFIX` must be set to `path/to/folder/with/application.conf`
+ `LOG_DIR` can be set to a directory where all the logs will be stored`


Here is a sample of what you need to add to bash_profile:

```
export HADOOP_HOME=/Users/Reyhaneh/hadoop/hadoop-dist/target/hadoop-2.7.2
export HADOOP_CONF_DIR=/Users/Reyhaneh/hadoop/config-files
export HADOOP_CLASSPATH=/Users/Reyhaneh/hadoop/hadoop-dist/target/*:$HADOOP_CONF_DIR
export LOG_DIR=$HADOOP_CONF_DIR/logs
```

## Running XTrace PubSub


All of the instrumentation libraries use a pub sub system to communicate.  Agents running in the instrumented Hadoop will receive commands over pubsub, and send their output back over the pubsub.  The pubsub project is in `tracingplane/pubsub`

The following command will start an X-Trace server which also includes a pub sub server:

	tracingplane/pubsub/target/appassembler/bin/server

To check which Java process are running, use the `jps` command.  This is an easy way to check if something has crashed.  You should expect to see a process called `StandaloneServer`.

## Running XTrace Backend

The PubSub can instead be run using the backend command which also starts a visualization server at localhost:4080.

This can be started as follows:

```
$ tracing/xtrace/server/target/appassembler/bin/backend
```

## Start HDFS

From the base directory for the Hadoop git repository, the built version of hadoop will be located in `hadoop-dist/target/hadoop-2.7.2`.  Within the build directory, `bin` contains various command line utilities, while `sbin` contains some useful scripts for starting and stopping processes.  Before starting HDFS, we must format its data dir:

```
$ $HADOOP_HOME/bin/hdfs --config $HADOOP_CONF_DIR namenode -format
$ $HADOOP_HOME/bin/hdfs --config $HADOOP_CONF_DIR datanode -format
```

Then start an HDFS NameNode and HDFS DataNode:

```
$ $HADOOP_HOME/sbin/hadoop-daemon.sh start namenode
$ $HADOOP_HOME/sbin/hadoop-daemon.sh start datanode
```

You should expect to see messages in the output stream along the lines of the following:

	Pivot Tracing initialized
	Resource reporting executor started
	ZmqReporter: QUEUE-disk -> queue
	ZmqReporter: DISK- -> disk
	ZmqReporter: CPU- -> cpu
	/META-INF/lib/libthreadcputimer.dylib extracted to temporary file /var/folders/d3/38f0syys5yjcvynz8p4n3t4w0000gn/T/jni_file_2410736199844535966.dll

Again, check which Java processes are running using the `jps` command.  In addition to `StandaloneServer`, you should expect to see `NameNode` and `DataNode`

For reference, HDFS runs a Web UI by default at [localhost:50070](http://localhost:50070).


## Generating Traces

Traces can be generating by performing simple filesystem operations on hdfs.

An example is as follows

```
$ $HADOOP_HOME/bin/hadoop fs -mkdir /tests
$ $HADOOP_HOME/bin/hadoop fs -ls /
```

Full list of operations can be found here : [operations](https://hadoop.apache.org/docs/r2.7.2/hadoop-project-dist/hadoop-common/FileSystemShell.html)


## Stop HDFS

To stop an HDFS NameNode and HDFS DataNode:

```
$ $HADOOP_HOME/sbin/hadoop-daemon.sh stop namenode
$ $HADOOP_HOME/sbin/hadoop-daemon.sh stop datanode
```
